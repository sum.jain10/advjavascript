// Javascript != JAVA
// It is a language which is inspired from Lisp , SmallTalk , Perl, Python, TCL, or Scheme
//  and has a syntactic similarity to Java

// In JavaScript, objects have a special hidden property 
// [[Prototype]] (as named in the specification), 
// that is either null or references another object. 
// That object is called “a prototype”:

//Example 

let animal = {
    eats: true
  };
let rabbit = {
  jumps: true
};
  
  rabbit.__proto__ = animal;


// console.log( rabbit.jumps ); 
rabbit.eats = false; 
console.log( animal.eats ); 
console.log( rabbit.eats ); 
rabbit.eats = true; 
animal.eats = false; 
console.log( animal.eats ); 
console.log( rabbit.eats ); 
//Add method in animal
// Override function in rabit