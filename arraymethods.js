// 1. Array.push()
// 2. Array.pop()
// 3. Array.shift()
// 4. Array.unshift()
// 5. Array.splice()
// 6. Array.slice(start, end)
// 7. Array.sort(); and how it works
// 8. forloop, foreach
// 9. map, filter, reduce
// 10. how to reverse string

// example:
let arr = ["t", "e", "f", "t"];
let a = [
    {
        name:"c"
    },
    {
        name:"b"
    },
    {
        name:"z"
    }
]
arr.push("y");
arr.pop();
arr.shift()
arr.unshift("a");

let newArraySplice=arr.splice(1,1)
console.log(newArraySplice, arr);
let subArray=arr.slice(1, 3);
console.log( subArray, arr );  
let names = ["Bilbo", "Gandalf", "Nazgul"];
let nameLength =[];
for(var i= 0;i<names.length;i++){
    nameLength.push(names[i].length);
}
arr.forEach((val,i,arr) => {
    console.log(val);
})
// map example=>
let lengths = ["Bilbo", "Gandalf", "Nazgul"].map(item => item.length);
console.log(lengths)

//filter example=>
let filteredData = lengths.filter(singleItem=> singleItem>5);

//Reduce example=>
let newArray = [1, 2, 3, 4, 5];
let result = newArray.reduce((sum, current) => sum + current, 0);

console.log(result); // 15

//sort array
let sortArray = [5, 2, 1, -10, 8];
// ... your code to sort it in the reverse order
console.log( sortArray ); // 8, 5, 2, 1, -10
