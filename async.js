function printName(name, cb) {
    var fullname = "Mr/Ms "+ name;
    setTimeout(() => {
        cb(fullname);
    }, 100);
}

    
console.log('Started');
printName('nitin', function something(output) {
console.log('Function returned ', output);
})
console.log('Ended');

// ==============================================
function orderPizza(quantity, specification, cb){
    setTimeout(function AcceptOrder(err, data) {
        if(err) {
            console.log('Handle error');
            return cb(err, null)
        }
        console.log('order has been accepted');

        setTimeout(function prepareOrder(err, data) {
            if(err) {
                console.log('Handle error');
                return cb(err, null)
            }
            console.log('Order has been prepared');
            
            setTimeout(function OrderDispatched(err, data) {
                if(err) {
                    console.log('Handle error');
                    return cb(err, null)
                }
                console.log('Order dispatched');
                
                setTimeout(function OrderDelivered(err, data){
                    if(err) {
                        console.log('Handle error');
                        return cb(err, null)
                    }
                    console.log('Order delivered');
                    cb(null, 'yummy i got pizza')
                    
                }, 1000);

            }, 1000);

        }, 1000);
        
    }, 1000);

}

// ===========================================================
function orderPizzaPromise(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            resolve('Order');
        }, 1000);
    })
}

orderPizzaPromise('1').then(function(data) {
    console.log(data)
}).catch(function(err) {
    console.log(err)
});

// =================================================================
function orderPizzaPromise(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            resolve('Order');
        }, 1000);
    })
}

function AcceptPizzaPromise(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            resolve('Accecpted');
        }, 1000);
    })
}

function preparePizzaPromise(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            resolve('prepare');
        }, 1000);
    })
}

function delivedPizzaPromise(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            resolve('delived');
        }, 1000);
    })
}


orderPizzaPromise('1')
.then(function(data) {
    console.log(data);
    return AcceptPizzaPromise(data)
}).then(function(data){
    console.log(data);
    return preparePizzaPromise(data)
}).then(function(data){
    console.log(data);
    return delivedPizzaPromise(data)
}).then(function(data){
    console.log("result",data)
}).catch(function(err) {
    console.log(err)
});

// ===========================================

function OrderJPizza(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            console.log("Order j");
            resolve('Order J');
        }, 7000);
    })
}

function OrderDPizza(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            console.log("Order d");
            resolve('Order J');
        }, 1000);
    })
}

function OrderPPizza(params) {
    return new Promise((resolve, reject) => {
        if(false) {
            reject('failed');
        }
        setTimeout(() => {
            console.log("Order p");
            resolve('Order J');
        }, 9000);
    })
}


Promise.all([OrderDPizza('something'), OrderJPizza('something'), OrderPPizza('something')])
.then(function(data){
    console.log(data)
})
.catch(function(err){
    console.log(err);
})


//======================================================================


async function doSomethingGood(params) {
    const order = await orderPizzaPromise('a');
    const accepted =  await AcceptPizzaPromise(order);
    const prepared = await preparePizzaPromise(accepted);
    const delived = await delivedPizzaPromise(prepared);
    console.log(delived);
}

await Promise.all([OrderDPizza('something'), OrderJPizza('something'), OrderPPizza('something')]);