// var video = {
//     "title":"a",
//     play(){
//         console.log(this);
//         console.log(`Video ${this.title} is playing`)
//     }
// }

// video.play();

var c1 = {
    "name":"a",
}

var c2 = {
    "name" : "b"
}

function getName(lastName){
    console.log(this);
    console.log(`${this.name} - ${lastName}`);
}

// var c1Func = getName.bind(c2);

getName.call(c2,"a")
getName.apply(c2,["a1"])
c1Func(" Hello")
