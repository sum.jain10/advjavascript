// function makeWorker() {
//     let name = "Pete";
  
//     return function() {
//       console.log(name);
//     };
//   }
  
//   let name = "John";
  
//   let work = makeWorker();

//   work(); 



  // // private variable
  function makeCounter() {
    let count = 0;
  
    return function() {
      return count++; // has access to the outer "count"
    };
  }
  
  let counter = makeCounter();
  let counter2 = makeCounter();
  
  console.log( counter() ); // 0
  console.log( counter() ); // 1
  console.log( counter() ); // 2
  console.log( counter2() ); // 2