let { Animal} = require("./animal");

  
  // Inherit from Animal by specifying "extends Animal"
  class Rabbit extends Animal {
    hide() {
      console.log(`${this.name} hides!`);
    }
    run(speed,carrot){
      this.speed += carrot;
      console.log("Inside rabbit")
      super.run(speed);
    }
  }
  
  let rabbit = new Rabbit("White Rabbit");
  
  rabbit.run(5,1); // White Rabbit runs with speed 5.
  rabbit.hide(); // White Rabbit hides!